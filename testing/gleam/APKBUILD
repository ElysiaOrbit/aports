# Contributor: rubicon <rubicon@mailo.com>
# Maintainer: Celeste <cielesti@protonmail.com>
pkgname=gleam
pkgver=0.32.1
pkgrel=0
pkgdesc="Statically-typed language that compiles to Erlang and JS"
url="https://gleam.run/"
# s390x, riscv64, ppc64le: ring
# armhf: error: Undefined temporary symbol .LBB88_2
arch="all !armhf !s390x !riscv64 !ppc64le"
license="Apache-2.0"
depends="erlang-dev"
makedepends="cargo cargo-auditable"
source="$pkgname-$pkgver.tar.gz::https://github.com/gleam-lang/gleam/archive/refs/tags/v$pkgver.tar.gz"

prepare() {
	default_prepare

	cargo fetch --target="$CTARGET" --locked
}

build() {
	cargo auditable build --frozen --release
}

check() {
	cargo test --frozen
}

package() {
	install -Dvm755 target/release/gleam -t "$pkgdir"/usr/bin/
}

sha512sums="
016e90f8adadc51adf303c43cf9405c05dcd55ea7be5e25a9fac2814b67ee70691b2f8a133115122e8bbfde20dfa131b19d948d201459cc6c3dbe7b7e81738da  gleam-0.32.1.tar.gz
"
